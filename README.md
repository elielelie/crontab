### Crontab 

PHP library to manage GNU/Linux cron jobs.

## Installation

The library can be installed using Composer.
```
composer require elielelie/crontab
```